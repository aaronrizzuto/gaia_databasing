import numpy as np
import pdb,glob,os,sys
import psycopg2 as pg2
import psycopg2.extensions as pg2ext
import psycopg2.extras as pg2extra
import pandas as pd

def grab_colnames(cur,tabname):
    cur.execute("SELECT * FROM %s LIMIT 0"% tabname)
    return [desc[0] for desc in cur.description]



def grab_tablesize(conn):
    cur = con.cursor()
    cur.execute("SELECT schemaname,relname,n_live_tup FROM pg_stat_user_tables ORDER BY n_live_tup DESC")
    return cur.fetchall()
    
def recarry_to_blank_table(cur,conn,data,tabname,justdata=True,extrainfo=''):
    '''
    Function to take a cursor connected to a database with a blank table (just and index column)
    and put a whole numpy.recarray into it  
    BASICALLY you want this first in postgresql
    create table tabname(index bigint) tablespace tablespacename;
    '''
    usetab = tabname
    ##now loop over a number of columns to add into the database
    cstart  = 'ALTER TABLE ' + usetab 
    cnames = data.dtype.names
    if justdata == False: ##initialize columns if asked to
        for i in range(len(data.dtype.names)):
            thiscolname = data.dtype.names[i]
            thiscoltype = str(data.dtype[i])
            if thiscoltype == 'float64' : ct = ' float8 '
            if thiscoltype == 'int64'   : ct = ' bigint '
            if thiscoltype == '|S22'    : ct = ' varchar(22) '
            thiscommand = cstart + ' ADD COLUMN ' + thiscolname + ct + ';'
            cur.execute(thiscommand)
            conn.commit()
        print 'Columns added correctly'
    print 'Now adding data'
    #'insert into testing (test1) VALUES (%s)'% (innumber))
    thenames = grab_colnames(cur,tabname)
    ccc = np.array(cnames)
    ccc[:] = '%s'
    subb =  '(%s,'+','.join(ccc)+')'
    namer = ' (index,'+','.join(cnames)+')'
    for i in range(len(data)):
        if np.mod(i+1,1000) == 0: print 'Up to ' + str(i+1) + ' out of ' + str(len(data))  +' '+ extrainfo
        ##make a huge command to insert a row
        thecom = 'insert into ' + tabname +  namer + ' VALUES ' + subb
        tmptmp = (i,)+data[i].astype(list)
        cur.execute(thecom ,tmptmp)
        conn.commit()
        ### now log the star Im up to:  

def search_on_radec(connector,tabname,ralims,declims,cnames,aspandas=False):
    ##make the cursor
    cur = connector.cursor(cursor_factory = pg2extra.RealDictCursor)##make a dictionary type cursor  

    ##generate the search command
    searchcommand = "SELECT * FROM " + tabname + " WHERE " + cnames[0]+" >= " + str(ralims[0]) + " AND " + cnames[0] +" <= " + str(ralims[1]) + "  AND " +cnames[1] +" >= " + str(declims[0]) + " AND " +cnames[1] +" <= " + str(declims[1])
    
    ##do the search
    print 'running search'
    cur.execute(searchcommand) ##THIS works
    print 'search done!'

    ##use pandas to turn this into a useful recarray seems to work pretty well
    if aspandas == False: searchdata = pd.DataFrame(cur.fetchall()).to_records()
    if aspandas == True : searchdata = pd.DataFrame(cur.fetchall())
    return searchdata
    
def genqueary(connector,tabname,columnlist,constraintlist,aspandas=False):
    '''
    General function to query a table in the database you are connected to
    connector : DB connector object from psycopg2
    tabname   : name of the relational table you want to query
    columnlist: tuple of the table column names you want as output, simplest is (*,) for all 
    constraintlist: tuple of constraints or () for none (might crash memory though)
    and example is  e.g., ('ra > 20.','parallax > 2.0',"hip != '77909'")
    aspandas [optional]: output as recarray by default, if True outputs pandas.dataframe
    '''
    
    ##make the cursor
    cur = connector.cursor(cursor_factory = pg2extra.RealDictCursor)##make a dictionary type cursor  

    ##generate the column outputs choices
    colpart = ''
    for i,col in enumerate(columnlist): 
        colpart += col
        if i < len(columnlist)-1: colpart += ', '
    searchcommand = "SELECT " + colpart + " FROM " + tabname 
    
    ##add all the constraints to the select command
    if len(constraintlist) != 0:
        searchcommand +=  " WHERE " 
        for i,cs in enumerate(constraintlist): 
            searchcommand += cs         
            if i < len(constraintlist)-1: searchcommand += " AND "
    
    ##run the search
    print 'running search'
    cur.execute(searchcommand) ##THIS works
    print 'search done!'

    ##use pandas to turn this into a useful recarray, seems to work pretty well
    if aspandas == False: searchdata = pd.DataFrame(cur.fetchall()).to_records()
    if aspandas == True : searchdata = pd.DataFrame(cur.fetchall())
    return searchdata
    
    
    
    