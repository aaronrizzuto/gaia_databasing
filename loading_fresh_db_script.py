import numpy as np
from readcol import readcol 
import pdb,os,sys,glob
import psycopg2 as pg2
import psycopg2_basfuncs as basfunc


try: 
    conn = pg2.connect(database='tgas' ,user='acr2877' ,host='localhost' ,password='3546daf$')
except:
    print 'I am unable to connect to the database!!??!!'
    pdb.set_trace()
    
print 'Connected to database!!'
cur = conn.cursor()

##here's a query
#cur.execute("""SELECT datname from tgas""")    
#rows = cur.fetchall()

rd_dir = os.getenv("HOME") + '/data_local/tgas/raw/'
rfiles = glob.glob(rd_dir  + '*csv')
firstfile = rfiles[0]

print 'Reading datafile'
fd = readcol(firstfile,fsep=',',asRecArray=True)
print 'First datafile read in!' + ' ' + firstfile
basfunc.recarry_to_blank_table(cur,conn,fd,'tgtesting',justdata=False,extrainfo=' f0')
pdb.set_trace()
rfiles = rfiles[1:]
for i in range(len(rfiles)):
    thisdata = readcol(rfiles[i],fsep=',',asRecArray=True)
    basfunc.recarry_to_blank_table(cur,conn,thisdata,'tgasfull',justdata=True,extrainfo=' f' + str(i+1))

    
    
    



    
pdb.set_trace()
print 'Im Done'



##GENERAL NOTES:
#OPEN postgres like: psql postgres -U user
#\connect databasename

##Databases have this heirarchy:
# database
# tables in the database
# columns in the tables
# data in the columns
#


#to save things make sure to do a conn.commit(), to rollback do conn.rollback()
