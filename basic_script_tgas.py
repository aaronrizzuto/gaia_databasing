import numpy as np
from readcol import readcol 
import matplotlib.pyplot as plt
import pdb,os,sys,glob
import psycopg2 as pg2
import psycopg2_basfuncs as basfunc
import psycopg2.extras as pg2extra

try: 
    conn = pg2.connect(database='tgas' ,user='acr2877' ,host='localhost' ,password='3546daf$')
except:
    print 'I am unable to connect to the database!!??!!'
    pdb.set_trace()
    
print 'Connected to database!!'

##we don't really need a cursor for now but here are some examples:
#cur = conn.cursor() ##standard cursor
#cur = conn.cursor(cursor_factory = pg2extra.RealDictCursor) ##cursor that outputs dictionaries

##here's a query
#cur.execute("SELECT colname from tgas WHERE colname > condition")  #do a query
#rows = cur.fetchall() ##grab the result

#lets try running a search and getting it into a form we want to use with codes later in python
##a search based on ra and dec locations for e.g., Upper Sco
#sco?
##this takes a few seconds to search ~2million rows
ralims  = [230.,250.]
declims = [-30.,-15.]

##another random range
#ralims  = [44.,46.]
#declims = [-99.,+99.]

##tell it the table and the columns you are using
tabname = 'tgasfull'
radec_cnames = ['ra','dec']

stuff = basfunc.search_on_radec(conn,tabname,ralims,declims,radec_cnames,aspandas=False)
    
##how about a more general type of search??
constraints = ('ra >= 230','ra <= 250','parallax > 4','parallax < 10','dec >= -30.','dec <= -15.')
outcols = ('*',)
stuff2 = basfunc.genqueary(conn,tabname,outcols,constraints,aspandas=False)

##and a targeted search
constraints = ("hip = '77909'",)
outcols = ('ra','dec','parallax','hip')
stuff3 = basfunc.genqueary(conn,tabname,outcols,constraints,aspandas=False)
    
pdb.set_trace()
print 'Im Done'



##GENERAL NOTES:
#OPEN postgres like: psql postgres -U user
#\connect databasename

##Databases have this heirarchy:
# database
# tables in the database
# columns in the tables
# data in the columns
#


#to save things make sure to do a conn.commit(), to rollback do conn.rollback()
